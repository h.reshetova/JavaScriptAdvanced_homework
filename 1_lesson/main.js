function setColor(){
	var toDel = document.getElementById('color');
	if (toDel){
		toDel.remove();
	}
	var color = '#';
	for (var i = 0; i < 3; i++){
		var num = Math.floor(Math.random() * (256 - 16 + 1)) + 16;
		color += num.toString(16);
	}
	document.body.style.backgroundColor = color;
	var wrapDiv = document.getElementById('app');
	var colorCaption = document.createElement('div');
	colorCaption.setAttribute('id', 'color');
	colorCaption.style.height = '100%';
	colorCaption.style.display = 'flex';
	colorCaption.style.justifyContent = 'center';
	colorCaption.style.alignItems = 'center';
	colorCaption.innerText = color;
	wrapDiv.appendChild(colorCaption);
}
